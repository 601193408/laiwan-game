package com.java80.littlegame.service.gateway.websocket;

import java.util.Properties;

import com.java80.littlegame.common.utils.LoadPropertiesFileUtil;

public class GatewayConfig {
	private static String queueName;
	private static String serviceId;
	private static int serverport;
	private static String instanceName;
	static {
		init();
	}

	public static void init() {
		Properties p = LoadPropertiesFileUtil.loadProperties("../config/cfg.properties");
		queueName = p.getProperty("service.gateway.self.queuename");
		serviceId = p.getProperty("service.gateway.id");
		instanceName = p.getProperty("service.gateway.insname");
		serverport = Integer.parseInt(p.getProperty("service.gateway.net.port"));
	}

	public static String getQueueName() {
		return queueName;
	}

	public static String getServiceId() {
		return serviceId;
	}

	public static int getServerport() {
		return serverport;
	}

	public static String getInstanceName() {
		return instanceName;
	}
}
