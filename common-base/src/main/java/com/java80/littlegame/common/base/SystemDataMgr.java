package com.java80.littlegame.common.base;

import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java80.littlegame.common.db.dao.dbImpl.SGameConfigDaoDBImpl;
import com.java80.littlegame.common.db.dao.dbImpl.SGameInfoDaoDBImpl;
import com.java80.littlegame.common.db.entity.SGameConfig;
import com.java80.littlegame.common.db.entity.SGameInfo;

public class SystemDataMgr {
	final transient static Logger log = LoggerFactory.getLogger(SystemDataMgr.class);
	private static TreeMap<Integer, SGameInfo> gameInfos = new TreeMap<>();
	private static TreeMap<Integer, SGameConfig> gameConfigs = new TreeMap<>();

	public static void loadSystemData() {
		List<SGameInfo> sgameInfos = new SGameInfoDaoDBImpl().getAll();
		for (SGameInfo i : sgameInfos) {
			gameInfos.put(i.getId(), i);
		}

		List<SGameConfig> sgameconfigs = new SGameConfigDaoDBImpl().getAll();
		for (SGameConfig i : sgameconfigs) {
			gameConfigs.put(i.getGameId(), i);
		}
	}

	public static TreeMap<Integer, SGameInfo> getGameInfos() {
		return gameInfos;
	}

	public static TreeMap<Integer, SGameConfig> getGameConfigs() {
		return gameConfigs;
	}

}
