package com.java80.littlegame.common.message.proto.game.caiquan;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

/**
 * 
 * 告诉客户端玩家的猜拳动作 server ->client
 *
 */
public class CaiquanActionResultMessage extends BaseMsg {
	private byte actionId;// 出拳动作
	private List<Result> results = new ArrayList<>();

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	public byte getActionId() {
		return actionId;
	}

	public void setActionId(byte actionId) {
		this.actionId = actionId;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_CAIQUANACTION_RESULT;
	}

	public static class Result {
		private byte result;// 结果 石头剪刀布
		private long userId;// 谁出的

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public byte getResult() {
			return result;
		}

		public void setResult(byte result) {
			this.result = result;
		}
	}

}
