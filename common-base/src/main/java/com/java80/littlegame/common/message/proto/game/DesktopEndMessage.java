package com.java80.littlegame.common.message.proto.game;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

/**
 * 
 * 通知客户端 桌子结束
 */
public class DesktopEndMessage extends BaseMsg {
	private int gameId;
	private int roomId;
	private int reason;// 1打完了，2超时

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_DESKTOPEND;
	}

}
