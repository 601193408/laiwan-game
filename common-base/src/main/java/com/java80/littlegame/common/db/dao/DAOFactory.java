package com.java80.littlegame.common.db.dao;

import com.java80.littlegame.common.db.dao.dbImpl.URoomInfoDaoDBImpl;
import com.java80.littlegame.common.db.dao.dbImpl.UUserInfoDaoDBImpl;
import com.java80.littlegame.common.db.dao.dbImpl.UUserRoomInfoDaoDBImpl;

public class DAOFactory {
	public static UUserInfoDao getUserInfoDao() {
		return new UUserInfoDaoDBImpl();
	}

	public static UUserRoomInfoDao getUserRoomInfoDao() {
		return new UUserRoomInfoDaoDBImpl();
	}

	public static URoomInfoDao getRoomInfoDao() {
		return new URoomInfoDaoDBImpl();
	}
}
