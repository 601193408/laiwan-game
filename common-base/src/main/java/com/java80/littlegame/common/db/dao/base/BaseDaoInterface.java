package com.java80.littlegame.common.db.dao.base;

public interface BaseDaoInterface<T> {
	public void insert(T t);

	public void delete(long id);

	public void update(T t);

	public T get(long id);

}
