package com.java80.littlegame.common.db.dao.base;

import java.util.List;

public interface BaseUserDaoInterface<T> extends BaseDaoInterface<T> {
	public List<T> getAllByUserId(long userId);
}
