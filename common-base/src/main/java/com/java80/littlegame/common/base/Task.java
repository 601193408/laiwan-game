package com.java80.littlegame.common.base;

public abstract class Task {
	public abstract void work();
}
