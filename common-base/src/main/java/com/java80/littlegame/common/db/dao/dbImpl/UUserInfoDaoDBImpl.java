package com.java80.littlegame.common.db.dao.dbImpl;

import java.util.Arrays;
import java.util.List;

import com.java80.littlegame.common.db.dao.UUserInfoDao;
import com.java80.littlegame.common.db.dao.base.BaseDao;
import com.java80.littlegame.common.db.entity.UUserInfo;

public class UUserInfoDaoDBImpl extends BaseDao implements UUserInfoDao {

	@Override
	public void insert(UUserInfo userinfo) {
		String sql = "insert into u_user_info values(?,?,?,?,now())";
		super.insert(sql, Arrays.asList(null, userinfo.getNickName(), userinfo.getLoginName(), userinfo.getPassword()));
	}

	@Override
	public void delete(long id) {
		super.delete("delete from u_user_info where id=?", Arrays.asList(id));
	}

	@Override
	public void update(UUserInfo userinfo) {
		super.update("update u_user_info set nickName=?,loginName=?,password=?",
				Arrays.asList(userinfo.getNickName(), userinfo.getLoginName(), userinfo.getPassword()));
	}

	@Override
	public List<UUserInfo> getAllByUserId(long userId) {
		return super.find("select * from u_user_info where userId=?", Arrays.asList(userId), UUserInfo.class);
	}

	@Override
	public UUserInfo login(String userName, String password) {
		List<UUserInfo> list = super.find("select * from u_user_info where loginName=? and password=?",
				Arrays.asList(userName, password), UUserInfo.class);
		if (list == null || list.isEmpty() || list.size() > 1) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public UUserInfo get(long id) {
		String sql = "select * from u_user_info where id=?";
		return super.get(sql, Arrays.asList(id), UUserInfo.class);
	}

	@Override
	public UUserInfo findByLoginName(String loginName) {
		String sql = "select * from u_user_info where loginName=?";
		return super.get(sql, Arrays.asList(loginName), UUserInfo.class);
	}

}
