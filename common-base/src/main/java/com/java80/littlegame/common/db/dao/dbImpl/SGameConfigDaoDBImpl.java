package com.java80.littlegame.common.db.dao.dbImpl;

import java.util.Arrays;
import java.util.List;

import com.java80.littlegame.common.db.dao.SGameConfigDao;
import com.java80.littlegame.common.db.dao.base.BaseSysDao;
import com.java80.littlegame.common.db.entity.SGameConfig;

public class SGameConfigDaoDBImpl extends BaseSysDao<SGameConfig> implements SGameConfigDao {
	@Override
	public List<SGameConfig> getAll() {
		return super.find("select * from s_game_config", Arrays.asList(), SGameConfig.class);
	}

}
