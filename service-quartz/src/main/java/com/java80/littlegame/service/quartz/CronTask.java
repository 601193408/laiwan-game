package com.java80.littlegame.service.quartz;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import com.java80.littlegame.common.base.VertxMessageHelper;

public class CronTask implements Serializable {
	private static final long serialVersionUID = 6000566929673475616L;
	public static final String TASK_KEY = "task_key";
	public static final String TASK_KEY_CRON = "task_key_cron";
	private static AtomicLong IDSource = new AtomicLong(1);
	/** 任务id **/
	private long id;
	/** 开始时间 **/
	private long startTime;
	private long endTime;
	private long delay;
	private String timerMsg;
	private JobDetail jobDetail;
	private Trigger trigger;
	private String cronType;
	private CronTaskMgr mgr;
	private String targetQueueName;

	public CronTask(long delay, String timerMsg, String targetQueueName) {
		this.id = IDSource.incrementAndGet();
		this.startTime = System.currentTimeMillis();
		this.endTime = startTime + delay;
		this.timerMsg = timerMsg;
		this.targetQueueName = targetQueueName;
	}

	public void start() {

	}

	public String getCronType() {
		return cronType;
	}

	public void setCronType(String cronType) {
		this.cronType = cronType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public String getTimerMsg() {
		return timerMsg;
	}

	public void setTimerMsg(String timerMsg) {
		this.timerMsg = timerMsg;
	}

	public void end() {
		// TODO Auto-generated method stub
		System.out.println(">>>>>>end()>>>>>>>>");
		if (StringUtils.isNotBlank(targetQueueName)) {
			VertxMessageHelper.sendMessageToService(targetQueueName, timerMsg);
		}
	}

	public CronTask reset() {

		this.startTime = System.currentTimeMillis();

		this.endTime = startTime + delay;

		return this;
	}

	public void setDetail(CronTaskMgr mgr, JobDetail jobDetail, Trigger trigger) {
		this.jobDetail = jobDetail;
		this.trigger = trigger;
		this.mgr = mgr;
	}

	public JobDetail getJobDetail() {
		return jobDetail;
	}

	public Trigger getTrigger() {
		return trigger;
	}

	public CronTaskMgr getMgr() {
		return mgr;
	}

}
